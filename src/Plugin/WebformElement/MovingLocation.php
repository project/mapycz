<?php

namespace Drupal\mapycz\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;

/**
 * Provides a 'moving_location' element.
 *
 * @WebformElement(
 *   id = "mapy_cz_moving_location",
 *   label = @Translation("MapyCz moving Location"),
 *   description = @Translation("Provides a custom element with two text fields for moving from and to locations, and one date field."),
 *   category = @Translation("Custom")
 * )
 */
class MovingLocation extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    return [
        'moving_from' => '',
        'moving_to' => '',
        'date' => '',
      ] + parent::getDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function getCompositeElements(): array {
    $elements = [];
    $elements['moving_from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Moving From'),
      '#required' => TRUE,
    ];
    $elements['moving_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Moving To'),
      '#required' => TRUE,
    ];
    $elements['date'] = [
      '#type' => 'date',
      '#title' => $this->t('Date'),
      '#required' => TRUE,
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function validateElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    parent::validateElement($element, $form_state, $complete_form);
    // Add custom validation if needed.
  }

}
