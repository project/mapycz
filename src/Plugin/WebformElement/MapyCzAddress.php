<?php

namespace Drupal\mapycz\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\TextField;

/**
 * Provides a 'mapy_cz_address' element.
 *
 * @WebformElement(
 *   id = "mapy_cz_address",
 *   label = @Translation("MapyCz address"),
 *   description = @Translation("Custom element with mapyCz address"),
 *   category = @Translation("Custom")
 * )
 */
class MapyCzAddress extends TextField {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties(): array {
    return [
      'address' => '',
    ] + parent::getDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function initialize(array &$element) {
    $unique_id = 'MapyCz-autoComplete-' . uniqid();

    $element['#attributes']['id'] = $unique_id;
    $element['#attributes']['class'][] = 'MapyCz-autoComplete';
    $element['#attached']['library'][] = 'mapycz/custom-autocomplete';
    $config = \Drupal::config('mapycz.settings');
    $element['#attached']['drupalSettings']['mapycz'] = [
      'api_key' => $config->get('api_key'),
      'unique_id' => $unique_id,
    ];
    parent::initialize($element);
  }

}
