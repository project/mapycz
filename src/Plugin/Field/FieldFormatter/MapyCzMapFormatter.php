<?php

namespace Drupal\mapycz\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mapycz\MapyCzCore;

/**
 * Plugin implementation of the 'mapycz_map' formatter.
 *
 * @FieldFormatter(
 *   id = "mapycz_map",
 *   module = "mapycz",
 *   label = @Translation("Mapy CZ - Map"),
 *   field_types = {
 *     "mapycz"
 *   }
 * )
 */
class MapyCzMapFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = [];

    $settings['width'] = '100%';
    $settings['height'] = '350px';
    $settings['type'] = 1;
    $settings['show_poi'] = 0;
    $settings['show_layer_switcher'] = 0;
    $settings['allowed_layers'] = [1 => 1];

    $settings += parent::defaultSettings();

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    $element['width'] = [
      '#title' => $this->t('Width'),
      '#type' => 'textfield',
      '#default_value' => $settings['width'],
      '#description' => $this->t('Enter size and unit, e.g. 200px or 100%.'),
    ];

    $element['height'] = [
      '#title' => $this->t('Height'),
      '#type' => 'textfield',
      '#default_value' => $settings['height'],
      '#description' => $this->t('Enter size and unit, e.g. 200px or 100%.'),
    ];

    $element['type'] = [
      '#title' => $this->t('Map type'),
      '#type' => 'select',
      '#options' => MapyCzCore::getMapTypeOptions(),
      '#default_value' => $settings['type'],
      '#description' => $this->t("Choose default map type to show. If map in a node has it's own type set, it will be used."),
    ];

    $element['show_poi'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show points of interest layer in the map'),
      '#default_value' => $settings['show_poi'],
      '#options' => [
        1 => t('Yes'),
        0 => t('No'),
      ],
    ];

    $element['show_layer_switcher'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show layer switcher'),
      '#default_value' => $settings['show_layer_switcher'],
      '#options' => [
        1 => t('Yes'),
        0 => t('No'),
      ],
    ];

    $element['allowed_layers'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed map layers'),
      '#default_value' => $settings['allowed_layers'],
      '#options' => MapyCzCore::getMapTypeOptions(),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();

    $summary = [];
    $summary[] = $this->t('Map type: @type', ['@type' => MapyCzCore::getMapTypeOptions()[$settings['type']]]);
    $summary[] = $this->t('Width: @width', ['@width' => $settings['width']]);
    $summary[] = $this->t('Height: @height', ['@height' => $settings['height']]);
    $summary[] = $this->t('Show POI: @show_poi', ['@show_poi' => $settings['show_poi'] ? $this->t('Yes') : $this->t('No')]);
    $summary[] = $this->t('Show layer switcher: @show_layer_switcher', ['@show_layer_switcher' => $settings['show_layer_switcher'] ? $this->t('Yes') : $this->t('No')]);
    $allowed_layers = array_intersect_key(MapyCzCore::getMapTypeOptions(), array_flip($settings['allowed_layers']));
    $summary[] = $this->t('Allowed layers: @layers', ['@layers' => implode(', ', $allowed_layers)]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();

    $element = [
      '#attached' => [
        'library' => [
          'mapycz/mapycz.frontend',
        ],
      ],
    ];

    foreach ($items as $delta => $item) {
      $map_id = uniqid('mapycz-widget-' . $delta);
      $element[$delta] = [
        '#theme' => 'mapycz_map',
        '#map_id' => $map_id,
        '#center' => [
          'lat' => $item->data['center_lat'],
          'lng' => $item->data['center_lng'],
        ],
        '#zoom' => $item->data['zoom'],
        '#type' => $item->data['type'] == 'default' ? $settings['type'] : $item->data['type'],
        '#markers' => [
          0 => [
            'lat' => $item->lat,
            'lng' => $item->lng,
          ],
        ],
        '#width' => $settings['width'],
        '#height' => $settings['height'],
        '#show_poi' => $settings['show_poi'],
        '#show_layer_switcher' => $settings['show_layer_switcher'],
        '#allowed_layers' => json_encode(array_flip(array_filter($settings['allowed_layers']))),
        '#theme_wrappers' => [
          'container' => [
            '#attributes' => [
              'id' => $map_id . '-wrapper',
              'class' => 'mapycz-wrapper',
            ],
          ],
        ],
      ];
    }

    return $element;
  }

}
