/**
 * @file
 * Mapycz backend.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.mapyczBehavior = {

    attach: function (context, settings) {
      var layers = {
        '1': 'Základní',
        '2': 'Turistická',
        '3': 'Satelitní',
      };
      Drupal.mapycz.mapsInit({ admin: true, suggest: true, layerOptions: layers }, context);
    }

  };

})(jQuery, Drupal);
